:css: css/style-presentation.css

:skip-help: true
:auto-console: true

----

:hovercraft-path: M200 200 S 199.6 200 199.3 200.1 198.9 200.1 198.6 200.2 198.2 200.3 197.9 200.5 197.6 200.7 197.2 200.9 196.9 201.1 196.7 201.3 196.4 201.6 196.1 201.9 195.9 202.2 195.6 202.5 195.4 202.9 195.3 203.3 195.1 203.7 195 204.1 194.8 204.5 194.8 204.9 194.7 205.4 194.7 205.8 194.6 206.3 194.7 206.8 194.7 207.3 194.8 207.8 194.9 208.3 195 208.8 195.2 209.3 195.4 209.8 195.6 210.3 195.9 210.8 196.2 211.3 196.5 211.7 196.9 212.2 197.3 212.7 197.7 213.1 198.1 213.5 198.6 214 199.1 214.4 199.6 214.8 200.2 215.1 200.8 215.5 201.4 215.8 202 216.1 202.7 216.3 203.4 216.6 204.1 216.8 204.8 217 205.6 217.1 206.3 217.2 207.1 217.3 207.9 217.4 208.7 217.4 209.5 217.4 210.4 217.3 211.2 217.2 212.1 217 212.9 216.9 213.8 216.6 214.6 216.4 215.5 216.1 216.3 215.7 217.2 215.3 218 214.9 218.9 214.4 219.7 213.9 220.5 213.4 221.3 212.8 222.1 212.1 222.8 211.5 223.6 210.7 224.3 210 225 209.2 225.7 208.3 226.3 207.5 226.9 206.6 227.5 205.6 228.1 204.6 228.6 203.6 229 202.6 229.5 201.5 229.9 200.4 230.2 199.2 230.5 198.1 230.8 196.9 231 195.7 231.2 194.5 231.3 193.2 231.4 191.9 231.4 190.7 231.4 189.4 231.3 188.1 231.1 186.8 230.9 185.4 230.7 184.1 230.4 182.8 230 181.5 229.6 180.2 229.1 178.8 228.6 177.5 228 176.2 227.3 175 226.6 173.7 225.9 172.4 225.1 171.2 224.2 170 223.2 168.8 222.3 167.7 221.2 166.6 220.1 165.5 219 164.4 217.8 163.4 216.5 162.4 215.2 161.5 213.9 160.6 212.5 159.8 211.1 159 209.6 158.3 208.1 157.6 206.5 156.9 205 156.4 203.3 155.8 201.7 155.4 200 155 198.3 154.7 196.6 154.4 194.8 154.2 193 154.1 191.2 154 189.4 154 187.6 154.1 185.8 154.3 183.9 154.5 182.1 154.8 180.3 155.2 178.4 155.6 176.6 156.2 174.8 156.8 173 157.4 171.2 158.2 169.4 159 167.7 159.9 166 160.9 164.3 161.9 162.6 163.1 161 164.3 159.4 165.5 157.8 166.9 156.3 168.3 154.8 169.7 153.4 171.3 152.1 172.9 150.8 174.5 149.5 176.2 148.3 178 147.2 179.9 146.1 181.7 145.1 183.7 144.2 185.7 143.4 187.7 142.6 189.8 141.9 191.9 141.3 194.1 140.7 196.3 140.3 198.5 139.9 200.8 139.6 203 139.4 205.3 139.3 207.7 139.3 210 139.3 212.4 139.5 214.7 139.7 217.1 140.1 219.5 140.5 221.8 141.1 224.2 141.7 226.6 142.4 228.9 143.2 231.2 144.1 233.5 145.1 235.8 146.2 238.1 147.4 240.3 148.7 242.5 150.1 244.6 151.5 246.7 153.1 248.8 154.7 250.8 156.4 252.7 158.2 254.6 160.1 256.4 162 258.2 164.1 259.9 166.2 261.5 168.4 263.1 170.6 264.5 172.9 265.9 175.3 267.3 177.8 268.5 180.2 269.6 182.8 270.7 185.4 271.6 188.1 272.5 190.8 273.2 193.5 273.9 196.3 274.4 199.1 274.9 201.9 275.2 204.7 275.5 207.6 275.6 210.5 275.6 213.4 275.5 216.3 275.3 219.2 275 222.2 274.5 225.1 274 228 273.3 230.9 272.6 233.7 271.7 236.6 270.7 239.4 269.5 242.2 268.3 244.9 267 247.6 265.5 250.3 264 252.9 262.3 255.4 260.5 257.9 258.7 260.4 256.7 262.7 254.6 265 252.4 267.2 250.2 269.4 247.8 271.4 245.3 273.4 242.8 275.3 240.2 277.1 237.5 278.7 234.7 280.3 231.8 281.8 228.9 283.2 225.9 284.5 222.8 285.6 219.7 286.6 216.5 287.6 213.3 288.4 210 289 206.7 289.6 203.4 290 200 290.3 196.6 290.5 193.2 290.5 189.7 290.4 186.3 290.2 182.8 289.8 179.3 289.3 175.9 288.7 172.4 287.9 169 287 165.5 286 162.1 284.8 158.8 283.5 155.4 282.1 152.1 280.5 148.9 278.9 145.7 277 142.5 275.1 139.4 273 136.4 270.9 133.5 268.6 130.6 266.1 127.8 263.6 125.1 260.9 122.4 258.2 119.9 255.3 117.5 252.4 115.1 249.3 112.9 246.2 110.8 242.9 108.8 239.6 106.9 236.2 105.1 232.7 103.5 229.1 102 225.5 100.6 221.8 99.4 218.1 98.3 214.3 97.3 210.4 96.5 206.6 95.8 202.6 95.3 198.7 94.9 194.7 94.7 190.7 94.6 186.7 94.6 182.7 94.9 178.6 95.2 174.6 95.8 170.6 96.4 166.6 97.3 162.7 98.3 158.7 99.4 154.8 100.7 150.9 102.2 147.1 103.8 143.3 105.5 139.6 107.4 136 109.5 132.4 111.7 128.9 114 125.4 116.5 122.1 119.1 118.8 121.8 115.7 124.7 112.6 127.7 109.7 130.8 106.8 134.1 104.1 137.4 101.5 140.9 99 144.5 96.7 148.2 94.5 152 92.4 155.8 90.5 159.8 88.7 163.8 87.1 168 85.6 172.2 84.3 176.4 83.1 180.7 82.1 185.1 81.3 189.5 80.6 194 80.1 198.5 79.8 203 79.6 207.6 79.7 212.1 79.8 216.7 80.2 221.3 80.7 225.9 81.4 230.4 82.3 235 83.4 239.5 84.6 244 86 248.5 87.6 252.9 89.4 257.2 91.3 261.6 93.4 265.8 95.6 270 98.1 274.1 100.6 278.1 103.4 282 106.3 285.8 109.3 289.5 112.5 293.2 115.9 296.7 119.3 300 122.9 303.3 126.7 306.4 130.6 309.4 134.5 312.3 138.7 315 142.9 317.5 147.2 319.9 151.6 322.2 156.1 324.2 160.8 326.2 165.4 327.9 170.2 329.5 175 330.8 179.9 332 184.9 333.1 189.9 333.9 194.9 334.5 200 335 205.1 335.3 210.2 335.3 215.4 335.2 220.5 334.9 225.6 334.4 230.8 333.7 235.9 332.8 241 331.7 246 330.4 251 328.9 256 327.2 260.9 325.3 265.7 323.2 270.5 321 275.2 318.5 279.8 315.9 284.4 313.1 288.8 310.1 293.1 307 297.3 303.7 301.4 300.2 305.4 296.5 309.2 292.7 312.9 288.7 316.5 284.6 319.9 280.4 323.2 276 326.2 271.5 329.2 266.8 331.9 262.1 334.5 257.2 336.9 252.2 339.1 247.1 341.1 242 343 236.7 344.6 231.4 346 226 347.3 220.5 348.3 215 349.1 209.4 349.7 203.8 350.1 198.1 350.3 192.4 350.3 186.7 350 181 349.5 175.4 348.9 169.7 348 164 346.8 158.3 345.5 152.7 344 147.1 342.2 141.6 340.2 136.2 338 130.8 335.7 125.4 333.1 120.2 330.3 115 327.3 110 324.1 105 320.7 100.2 317.1 95.5 313.3 90.9 309.4 86.4 305.3 82.1 301 78 296.5 73.9 291.9 70.1 287.1 66.4 282.2 62.9 277.2 59.6 272 56.5 266.7 53.5 261.3 50.8 255.7 48.3 250.1 45.9 244.3 43.8 238.5 41.9 232.6 40.2 226.6 38.7 220.5 37.5 214.4 36.5 208.3 35.7 202.1 35.1 195.8 34.8 189.6 34.7 183.3 34.9 177.1 35.3 170.8 35.9 164.6 36.8 158.4 37.9 152.2 39.2 146 40.8 140 42.6 133.9 44.6 128 46.9 122.1 49.4 116.3 52.1 110.6 55.1 105 58.3 99.5 61.7 94.1 65.3 88.9 69.1 83.8 73.1 78.8 77.3 74 81.7 69.4 86.3 64.9 91.1 60.6 96 56.5 101.2 52.6 106.4 48.8 111.9 45.3 117.5 42 123.2 38.9 129.1 36 135.1 33.3 141.2 30.9 147.4 28.7 153.7 26.7 160.1 25 166.6 23.5 173.2 22.2 179.8 21.2 186.5 20.5 193.2 20.5 193.2

:id: title

.. title: Analisi e sniffing di rete

.. image:: imgs/linuxvar.png

==========================
Analisi e sniffing di rete
==========================

di Riccardo Macoratti
=====================

This network analysis tutorial by Riccardo Macoratti is licensed under a
`Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
License`_.

.. _Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License: http://creativecommons.org/licenses/by-nc-sa/4.0/

.. image:: imgs/cc.png

----

=================
Requisiti di base
=================

- Sapere cos'è una rete
- Protocollo **ISO/OSI**
- Un minimo di conoscenza delle periferiche e dispositivi hardware e software
  impiegati (RJ45, protocollo Ethernet, router, switch, gateway, IP,
  MAC Address, servizi canonici come HTTP, DNS, MAIL, FTP...)

----

.. image:: imgs/boromir.jpg
  :width: 100%
  :align: left

----

================
Un po' di teoria
================

----

Definizione di rete
===================

Ripassiamo ora la definizione di una rete di calcolatori:

  "una rete di calcolatori è un'insieme di nodi di elaborazione totalmente
  autonomi tra loro, connessi mediante un opportuno sistema di comunicazione, ed
  in grado di interagire mediante scambio di messaggi al fine di condividere le
  risorse messe a disposizione da ciascon nodo"

.. figure:: imgs/rete.png
  :width: 250px
  :align: center

  Uno schema per visualizzare cos'è una rete. (Wikipedia__)

.. __: https://it.wikipedia.org/wiki/Topologia_di_rete

----

Com'è fisicamente fatta una rete
================================

Come ci si deve immaginare una rete?

.. figure:: imgs/rete-base.png
  :width: 820px
  :align: center
  
  Due calcolatori collegati da una rete. Al centro ci può essere uno switch, un
  router oppure (con particolari accorgimenti) anche nulla!

----

Modello ISO/OSI
===============

.. figure:: imgs/iso-osi.png
  :width: 400px
  :align: center

  Il diagramma ISO/OSI, che mostra l'incapsulamento dei vari protocolli, accanto
  ad una specifica per il protocollo TCP. (Wikipedia__)

.. __: https://it.wikipedia.org/wiki/Suite_di_protocolli_Internet

----

=================
Un po' di pratica
=================

----

``ip`` (``ifconfig``)
=====================

``ip`` è la nuova versione del comando ``ifconfig`` che era presente nelle
versioni più datate dei sistemi operativi Linux. Questo comando serve per
compiere un gran numero di azioni sulle interfacce di rete [#]_. Le
interfacce di rete sono tante quante sono le schede di rete (ossia quelle
periferiche che ci connettono ad un'altra macchina o ad Internet), più una
detta di *loopback*, che è associata alla nostra macchina e rappresenta una
sorta di specchio (tutto quello che le inviamo ci viene restituito nella stessa
maniera.

.. [#] Un'interfaccia di rete è il corrispettivo della scheda di rete,
  analizzata dal punto di vista del sistema operativo.

Esempi
------

.. code:: console

  # mostrare tutte le interfacce di rete
  $ ip addr
  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default 
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
  2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      link/ether 30:52:cb:83:fc:07 brd ff:ff:ff:ff:ff:ff
      inet 192.168.1.143/24 brd 192.168.1.255 scope global eth0
         valid_lft forever preferred_lft forever

.. code:: console

  # aggiungere un cammino statico verso indirizzi ip multicast
  $ ip route add 224.0.0.0/4 dev eth0

----

``ss`` (``netstat``)
====================

``ss`` è la nuova versione del comando ``netstat``, che era presente nelle
vecchie versioni dei sistemi Linux. Questo comando serve per analizzare le
connessioni in entrata e in uscita nella nostra macchina. Riporta una lista di
connessioni aperte riguardanti tutti i protocolli, anche quelli interni al
sistema operativo. Tramite delle impostazioni è possibile filtrare la lista per
mostrare solo le reali connessioni verso altre macchine (spesso nei protocolli
TCP e UDP).

Esempi
------

.. code:: console

  # mostrare tutte le connessioni in atto
  $ ss
  Netid State     Recv-Q Send-Q     Local Address:Port       Peer Address:Port
  u_str ESTAB     0      0          * 25096                  * 25098
  u_str ESTAB     0      0          * 26081                  * 26082
  u_str ESTAB     0      0          /run/user/1000/bus 25117 * 25116
  u_str ESTAB     0      0          * 24099                  * 24100
  u_str ESTAB     0      0          /run/user/1000/bus 25999 * 23949
  # [...]

----

.. code:: console

  # mostrare solo le connessioni che usano il protocollo IPv4
  $ ss -4
  Netid State      Recv-Q Send-Q  Local Address:Port    Peer Address:Port
  tcp   CLOSE-WAIT 1      0       159.149.248.41:51912  52.21.197.186:https
  tcp   ESTAB      0      0       159.149.248.41:48320  108.160.163.110:https
  tcp   LAST-ACK   1      1       159.149.248.41:22622  54.174.160.243:https
  tcp   CLOSE-WAIT 32     0       159.149.248.41:41624  54.192.1.110:https
  tcp   CLOSE-WAIT 32     0       159.149.248.41:36258  108.160.172.193:https
  tcp   ESTAB      0      0       159.149.248.41:47954  74.125.71.120:http
  tcp   CLOSE-WAIT 32     0       159.149.248.41:64838  108.160.172.204:https
  tcp   ESTAB      0      0       159.149.248.41:18266  216.58.212.66:https

.. code:: console

  # mostrare solo le connessioni che usano il protocollo IPv4, il processo
  # associato e sia in entrata sia in uscita
  $ ss -nap4
  Netid State      Recv-Q Send-Q  Local Address:Port    Peer Address:Port
  udp   UNCONN     0      0       *:68                  *:*
  tcp   LISTEN     0      128     127.0.0.1:17603       *:*                  users:(("dropbox",pid=10535,fd=65))
  tcp   LISTEN     0      128     *:17500               *:*                  users:(("dropbox",pid=10535,fd=59))
  tcp   LISTEN     0      128     127.0.0.1:17600       *:*                  users:(("dropbox",pid=10535,fd=61))
  tcp   CLOSE-WAIT 32     0       159.149.248.41:40642  54.192.1.110:443     users:(("dropbox",pid=10535,fd=37))
  tcp   ESTAB      0      0       159.149.248.41:48320  108.160.163.110:443  users:(("dropbox",pid=10535,fd=66))
  tcp   CLOSE-WAIT 32     0       159.149.248.41:36258  108.160.172.193:443  users:(("dropbox",pid=10535,fd=38))
  tcp   ESTAB      0      0       159.149.248.41:18258  216.58.212.66:443    users:(("firefox",pid=10544,fd=61))
  tcp   CLOSE-WAIT 32     0       159.149.248.41:40646  54.192.1.110:443     users:(("dropbox",pid=10535,fd=58))
  tcp   CLOSE-WAIT 32     0       159.149.248.41:64838  108.160.172.204:443  users:(("dropbox",pid=10535,fd=23))
  tcp   CLOSE-WAIT 1      0       159.149.248.41:47780  54.209.0.118:443     users:(("dropbox",pid=10535,fd=57))

----

``traceroute``
==============

``traceroute``, come dice il nome stesso, è una utility che permette di
visualizzare il percorso di un pacchetto all'interno di una rete IP. Ogni cambio
di direzione rappresenta un **router** [#]_, definito in quest'ambito *hop*
(salto). Tramite questo programma si può avere anche un'analisi dei ritardi di
trasmissione del pacchetto, per esempio per capire qual è il "collo di
bottiglia" della rete in oggetto.

.. [#] Un **router** è un particolare tipo di elaboratore che ha il compito di
  instradare un pacchetto verso la destinazione desiderata, che è contenuta nei
  metadati del pacchetto.

Esempi
------

.. code:: console

  # mostra gli hop per raggiungere Google Italia
  $ traceroute -4 -q 1 www.google.it
  traceroute to www.google.it (172.217.16.3), 30 hops max, 60 byte packets
   1  gateway (192.168.1.1)  1.093 ms
   2  151.7.202.72 (151.7.202.72)  10.896 ms
   3  151.7.80.88 (151.7.80.88)  11.220 ms
   4  151.7.80.10 (151.7.80.10)  14.466 ms
   5  151.6.5.230 (151.6.5.230)  17.189 ms
   6  MISG-B01-Ge10-1.wind.it (151.6.2.54)  19.940 ms
   7  151.6.0.30 (151.6.0.30)  23.128 ms
   8  209.85.241.94 (209.85.241.94)  25.001 ms
   9  64.233.174.245 (64.233.174.245)  40.682 ms
  10  mil02s06-in-f3.1e100.net (172.217.16.3)  32.592 ms

----

``tcpdump`` e ``ping``
======================

``ping`` (Packet INternet Groper) è una utility che invia pacchetti ICMP [#]_
verso un altro dispositivo in rete, misurandone il Round Trip Time [#]_ e
l'effettivo ritorno del pacchetto.

``tcpdump`` è l'utensile più basico per effettuare analisi di rete: cattura e
mostra i pacchetti TCP/IP in transito in rete. Può analizzare sia quelli in
entrata che quelli in uscita, ma spesso viene usato solo per i primi. Si adatta
bene ad essere usato in combinazione con ``ping``.

.. [#] ICMP (Internet Control Message Protocol) è un particolare tipo di
  protocollo che permette di inviare messaggi di controllo verso altri
  dispositivi in rete.

.. [#] Il Round Trip Time (RTT) è il tempo che un pacchetto impiega per arrivare
  a destinazione e tornare indietro.

Esempi
------

Prendiamo una caso base: capire se due macchine riescono a comunicare (in
maniera soddisfacente) tra loro in una rete sconosciuta. Poniamo che le due
macchine siano fisicamente molto lontane.

----

Le due macchine dovranno scambiarsi dei pacchetti e ci sarà bisogno di una
conferma di ricezione.
Riportiamo l'esempio sulla macchina che abbiamo a disposizione utilizzando
l'interfaccia di *loopback*.

Iniziamo ``ping``-ando la nostra stessa macchina.

.. code:: console

  $ ping localhost
  PING localhost.localdomain (127.0.0.1) 56(84) bytes of data.
  64 bytes from localhost.localdomain (127.0.0.1): icmp_seq=1 ttl=64 time=0.040 ms
  64 bytes from localhost.localdomain (127.0.0.1): icmp_seq=2 ttl=64 time=0.027 ms
  64 bytes from localhost.localdomain (127.0.0.1): icmp_seq=3 ttl=64 time=0.050 ms
  64 bytes from localhost.localdomain (127.0.0.1): icmp_seq=4 ttl=64 time=0.054 ms
  64 bytes from localhost.localdomain (127.0.0.1): icmp_seq=5 ttl=64 time=0.051 ms
  # [...]

Ora impostiamo ``tcdump`` per captare pacchetti ICMP (solo numero 8 che
corrisponde ai pacchetti inviati da ``ping`` attraverso l'interfaccia di
*loopback*.

.. code:: console

  $ tcpdump -nni lo -e icmp[icmptype]==8
  tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
  listening on lo, link-type EN10MB (Ethernet), capture size 262144 bytes
  12:47:35.295384 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 127.0.0.1 > 127.0.0.1: ICMP echo request, id 17854, seq 1, length 64
  12:47:36.295543 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 127.0.0.1 > 127.0.0.1: ICMP echo request, id 17854, seq 2, length 64
  12:47:37.295540 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 127.0.0.1 > 127.0.0.1: ICMP echo request, id 17854, seq 3, length 64
  12:47:38.295560 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 127.0.0.1 > 127.0.0.1: ICMP echo request, id 17854, seq 4, length 64
  12:47:39.295532 00:00:00:00:00:00 > 00:00:00:00:00:00, ethertype IPv4 (0x0800), length 98: 127.0.0.1 > 127.0.0.1: ICMP echo request, id 17854, seq 5, length 64

----

``drill`` (``dig``)
=====================

``drill`` è la versione più recente del comando ``dig`` che si trovava nelle
versioni più vecchie dei sistemi unix-like. Questo comando permette di
richiedere ad un server DNS (molto spesso quello associato alla nostra
connessione ad Internet) ogni sorta di informazioni. Cosa significa chiedere
informazioni al DNS? Significa risolvere quello che si chiama nome di dominio,
del tipo *www.example.com*, in un valore numero, cioè l'indirizzo IP [#]_.

.. [#] Un indirizzo IP è una 4-upla di quattro numeri che spaziano da 0 a 255,
  quindi 2\ :sup:`8`. Ogni numero è rappresentabile con 8 bit, perciò un
  byte. Un indirizzo IP "pesa" quindi 4 byte.

Esempi
------

.. code:: console

  # vogliamo sapere l'IP dell'host www.google.it (record di tipo A)
  $ drill www.google.it
  ;; ->>HEADER<<- opcode: QUERY, rcode: NOERROR, id: 9773
  ;; flags: qr rd ra ; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0 
  ;; QUESTION SECTION:
  ;; www.google.it. IN  A

  ;; ANSWER SECTION:
  www.google.it.  13  IN  A 216.58.212.99

  ;; AUTHORITY SECTION:

  ;; ADDITIONAL SECTION:

  ;; Query time: 97 msec
  ;; SERVER: 172.28.172.1
  ;; WHEN: Mon Mar  7 17:37:18 2016
  ;; MSG SIZE  rcvd: 47

----

.. code:: console

  # vogliamo sapere tutto il percorso della stessa richiesta DNS verso
  # i vari server
  $ drill -T www.google.it       
  it. 172800  IN  NS  m.dns.it.
  it. 172800  IN  NS  dns.nic.it.
  it. 172800  IN  NS  nameserver.cnr.it.
  it. 172800  IN  NS  a.dns.it.
  it. 172800  IN  NS  s.dns.it.
  it. 172800  IN  NS  r.dns.it.
  google.it.  10800 IN  NS  ns3.google.com.
  google.it.  10800 IN  NS  ns4.google.com.
  google.it.  10800 IN  NS  ns1.google.com.
  google.it.  10800 IN  NS  ns2.google.com.
  google.it.  10800 IN  NS  ns3.google.com.
  google.it.  10800 IN  NS  ns4.google.com.
  google.it.  10800 IN  NS  ns1.google.com.
  google.it.  10800 IN  NS  ns2.google.com.
  ns3.google.com.google.it. 10800 IN  NS  ns3.google.com.
  google.it.  10800 IN  NS  ns4.google.com.
  google.it.  10800 IN  NS  ns1.google.com.
  google.it.  10800 IN  NS  ns2.google.com.
  ns4.google.com.google.it. 10800 IN  NS  ns3.google.com.
  google.it.  10800 IN  NS  ns4.google.com.
  google.it.  10800 IN  NS  ns1.google.com.
  google.it.  10800 IN  NS  ns2.google.com.
  ns1.google.com.google.it. 10800 IN  NS  ns3.google.com.
  google.it.  10800 IN  NS  ns4.google.com.
  google.it.  10800 IN  NS  ns1.google.com.
  google.it.  10800 IN  NS  ns2.google.com.
  ns2.google.com.www.google.it. 300 IN  A 216.58.212.67

----

``nmap`` e Zenmap
=================

``nmap`` è il coltellino svizzero delle utility di analisi di rete in ambiente
Linux. Può scansionare una rete locale o remota e, per ogni dispositivo
incontrato, può effettuare vari test sulle porte software aperte o il
riconoscimento del sistema operativo. Nasce come utility a riga di comando.

Per facilitare l'uso di un'applicazione complessa come ``nmap``, gli stessi
sviluppatori hanno deciso di dotarla di una semplice, ma efficace, interfaccia
grafica, creando Zenmap. Tra le feature aggiunte viene annoverato una mappa
grafica dei dispositivi analizzati fino al momento della visualizzazione.

Esempi
------

.. code:: console

  # scansiona tutte le porte TCP sulla classe C (255 host) della sottorete
  # specificata, cercando di rilevarne l'os (e ignorando il blocco ping)
  $ sudo nmap -sS -Pn 192.168.1.0/24

  Starting Nmap 7.01 ( https://nmap.org ) at 2016-03-01 00:58 CET
  Nmap scan report for 192.168.1.1
  Host is up (0.0039s latency).
  Not shown: 994 closed ports
  PORT      STATE SERVICE
  23/tcp    open  telnet
  80/tcp    open  http
  139/tcp   open  netbios-ssn
  445/tcp   open  microsoft-ds
  1900/tcp  open  upnp
  49152/tcp open  unknown
  MAC Address: 10:FE:ED:F0:58:5E (Tp-link Technologies)

----

.. code:: console

  Nmap scan report for 192.168.1.3
  Host is up (0.016s latency).
  Not shown: 992 closed ports
  PORT     STATE SERVICE
  21/tcp   open  ftp
  22/tcp   open  ssh
  80/tcp   open  http
  1755/tcp open  wms
  4444/tcp open  krb524
  6666/tcp open  irc
  6881/tcp open  bittorrent-tracker
  9999/tcp open  abyss
  MAC Address: 00:01:E3:0F:37:CA (Siemens AG)

  Nmap scan report for 192.168.1.4
  Host is up (0.012s latency).
  Not shown: 999 closed ports
  PORT   STATE SERVICE
  80/tcp open  http
  MAC Address: 84:1B:5E:4A:06:CC (Netgear)

  Nmap scan report for 192.168.1.5
  Host is up (0.011s latency).
  Not shown: 998 closed ports
  PORT   STATE SERVICE
  53/tcp open  domain
  80/tcp open  http
  MAC Address: 00:01:E3:0F:37:CA (Siemens AG)

----

.. code:: console

  Nmap scan report for 192.168.1.6
  Host is up (0.011s latency).
  Not shown: 996 closed ports
  PORT     STATE SERVICE
  22/tcp   open  ssh
  80/tcp   open  http
  111/tcp  open  rpcbind
  9090/tcp open  zeus-admin
  MAC Address: 00:01:E3:0F:37:CA (Siemens AG)

  Nmap scan report for 192.168.1.11
  Host is up (0.013s latency).
  All 1000 scanned ports on 192.168.1.11 are closed
  MAC Address: 98:4B:4A:00:17:69 (Arris Group)

  Nmap scan report for 192.168.1.18
  Host is up (0.0065s latency).
  All 1000 scanned ports on 192.168.1.18 are closed
  MAC Address: 20:02:AF:BC:B3:D2 (Murata Manufacturing)

  Nmap scan report for 192.168.1.121
  Host is up (0.022s latency).
  Not shown: 998 filtered ports
  PORT      STATE SERVICE
  1234/tcp  open  hotline
  49153/tcp open  unknown
  MAC Address: 02:19:FB:50:C7:D8 (Unknown)

  Nmap scan report for 192.168.1.16
  Host is up (0.0000090s latency).
  All 1000 scanned ports on 192.168.1.16 are closed

  Nmap done: 256 IP addresses (9 hosts up) scanned in 37.48 seconds

.. code:: bash

  # ora provare ad inserire lo stesso comando all'interno di Zenmap

  # una volta finita la procedura di scan, visualizzare la tab 'topologia' e
  # osservare l'utile schema prodotto (consiglio: attiva il fish-eye)

----

Wireshark
=========

.. figure:: imgs/wireshark.png
  :width: 100%
  :align: center
  
  L'interfaccia grafica di Wireshark, utile complemento alle sue funzionalità.

----

Finiamo parlando di Wireshark, l'analizzatore di traffico open source più diffuso al mondo, ormai quasi uno standard.

Cosa fa?
  Cattura ogni singolo pacchetto transitante per la rete della quale la nostra
  macchina fa parte e lo analizza riportandoci particolari informazioni.

Perchè?
  Poniamo, esempio, di avere a che fare con una rete che risulta molto intasata
  e doverne capire la ragione. Iniziamo analizzando i pacchetti in transito e
  osserviamo che un host all'interno della rete invia una quantità sospetta di
  pacchetti di un certo tipo. Spegnendolo risolveremmo il problema, ma più
  efficientemente possiamo provare a capire il tipo di pacchetti inviati e
  dedurre la ragione di una tale quantità. In questa maniera possiamo agire sul
  software che invia questi pacchetti e farlo smettere.

  Poniamo, altro esempio, di dover analizzare le prestazioni di un sotware di
  streaming video. Sarebbe utile considerare la grandezza e il numero di
  pacchetti presenti in rete, ma abbiamo bisogno di analizzarli e filtrare solo
  quelli appartenenti al software di streaming.

In che modo?
  Viene attivata la **modalità promiscua** [#]_ dell'interfaccia di rete.

.. [#] La modalità promisqua ordina alla scheda di rete di considerare tutti i
  pacchetti che transitano sulla rete, contrariamente a quanto accade in una
  situazione normale, nella quale solo i pacchetti destinati al nostro indirizzo
  vengono presi in considerazione.

----

=====================================================
Quando non si ha una macchina Linux a portata di mano
=====================================================

----

Capitano spesso situazioni in cui bisogna testare l'effettivo funzionamento di
una rete e non si ha a disposizione un elaboratore esterno alla rete stessa,
oppure non se ne ha disponibile uno con un ambiente Linux consono. Vediamo cosa
è possibile fare in questi casi.

Se dobbiamo effettuare dei test generici quali ricerca del nostro IP, *whois*,
*reverse whois*, *speedtest* oppure operare con i server DNS, esistono dei
comodi tool online (e per giunta gratuitamente utilizzabili).

- `Domain Tools`__: 
  Questo servizio web permette di effettuare svariati test online: *whois*,
  *reverse whois*, *reverse IP, NS, MX lookup*, ricercare il proprio IP e
  operare coi DNS.

.. __: http://whois.domaintools.com/

- `you get signal`__: 
  Quest'altro servizio invece offre diversi tipi di ricerche, anche più
  avanzate: check delle porte software aperte verso Internet, ricerca del
  proprio IP, geolocazione di una rete, IP, numero di telefono, *traceroute*
  visuale, *reverse mail lookup* e *whois*.

.. __: http://www.yougetsignal.com/

- `DSLReports`__: 
  Un servizio non ottenibile localmente (e quindi utilizzabile esclusivamente
  via web), ma estremamente utile in alcuni casi è il test della velocità della
  connessione che si ha sotto mano al momento. Questo sito offre esattamente
  questo servizio.

.. __: http://www.dslreports.com/speedtest

----

.. figure:: imgs/doge.jpg
  :align: center

  Domande?
